import { useEffect, useState } from 'react';
import './App.css';
import ZxcvbnPassword from './components/zxcvbn/ZxcvbnPassword';
import MainMessage from './components/messages/MainMessage';
import { useNotification } from './components/messages/MessageProvider';
import message from './components/messages/message.json'


function App() {
 
  const dispatch = useNotification();

  return (
    
    <div className="App">
      <ZxcvbnPassword/>
      <br></br>
     <MainMessage/>
     <br></br>
     <button onMouseOver={() => dispatch(message.Messages.error)}>Another Message button</button>
    </div>

  );
}

export default App;
