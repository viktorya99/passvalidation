import React, {useState} from 'react';

function Password() {

    const [show, setShow] = useState(false);

    const valid = (item)=> {
        let text = document.querySelector(`#${item}`);
        text.style.color = "green";
    }
    const invalid = (item)=> {
        let text = document.querySelector(`#${item}`);
        text.style.color = "red";
    }

    const handleInputChange = (e) =>{
        const text = e.target.value;

        if(text.match(/[A-Z]/ )  !=null){
            valid('capital');
        }else{
            invalid('capital');
        }

        if(text.match(/[0-9]/ )  !=null){
            valid('num');
        }else{
            invalid('num');
        }

        if(text.match(/[!@#$%^&*]/ )  !=null){
            valid('specialchar');
        }else{
            invalid('specialchar');
        }

        if(text.length > 7){
            valid('more8');
        }else{
            invalid('more8');
        }
    };

    const handleShowHide = () =>{
        //show or hide password
        setShow(!show);
    }


  return <div>
      <div className='container'>
          <h2>this is js with react</h2>
          <input 
          type={show ?'text' :'password'}
          className='password'
          placeholder='Enter your password'
          onChange={handleInputChange}
          />
          
          {
              show ?(
                <button id='show_hidden' onClick={handleShowHide}>hide pass</button>
              ):(
                <button id='show_hidden' onClick={handleShowHide}>see pass</button>
              )
          }
          <p id='capital'> 
            <span>Capital letters</span>
          </p>

          <p id='specialchar'> 
            <span>Special characters</span>
          </p>

          <p id='num'> 
            <span>Use numbers</span>
          </p>

          <p id='more8'> 
            <span>8+ characters</span>
          </p>


      </div>
  </div>;
}

export default Password;
