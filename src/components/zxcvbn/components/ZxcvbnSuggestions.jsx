import React from 'react'

function ZxcvbnSuggestions(props) {
    console.log(props)
  return (
    <div>
        {props.length > 0 ?
        <ul>{props.suggestions.map(sugg =>  <li key={sugg}>{sugg}</li>)}</ul>
        :""}
    </div>
  )
}

export default ZxcvbnSuggestions