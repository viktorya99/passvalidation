import React from 'react';


const PassStrenghtMeter = (zxcvbnData) => {
    
    const DisplyPassLabel =()=>{
        switch(zxcvbnData.pass){
            case 0:
                return 'Very weak';
            case 1:
                return 'Weak';
            case 2:
                return 'Fair';
            case 3:
                return 'Good';
            case 4:
                return 'Strong ';
            default:
                return 'none';
        }
    }
    const div = <div class="strength gray-bck marginRight4"></div>;

  return(
    <>
    <p className='top-message'> Password reliability : <span className={`good-${zxcvbnData.pass}`}> {DisplyPassLabel()}( {zxcvbnData.pass +1} out of 5</span>)</p>
       <div class="strength-group">
        {zxcvbnData.pass>=0?<div class="strength strength-0 marginRight4"></div>: div}
        {zxcvbnData.pass>=1?<div class="strength strength-1 marginRight4"></div>: div}
        {zxcvbnData.pass>=2?<div class="strength strength-2 marginRight4"></div>: div}
        {zxcvbnData.pass>=3?<div class="strength strength-3 marginRight4"></div>: div}
        {zxcvbnData.pass>=4?<div class="strength strength-4 marginRight4"></div>: div}
    </div>
    <p className='bottom-message'>Password must have a minimum  rating of 3 out of 5 in order to be accepted </p>
  </>
  )
};

export default PassStrenghtMeter;




