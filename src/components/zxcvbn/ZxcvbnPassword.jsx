import React, { useState } from 'react';
import PassStrenghtMeter from './components/PassStrenghtMeter';
import zxcvbn from 'zxcvbn'; // -> npm install 
import ZxcvbnSuggestions from './components/ZxcvbnSuggestions';
import ZxcvbInput from './components/ZxcvbInput';
// import { createPopper } from "@popperjs/core"; // -> npm install
// import { v4 } from 'uuid';

function ZxcvbnPassword() {

  const [zxcvbnData, setZxcvbnData] = useState({
    password:'',
    score: (-1),
    suggestions: [],
  })

  function handleChange(e) {
    let passStrength = zxcvbn(zxcvbnData.password)
    setZxcvbnData(prevStrength => ({
      ...prevStrength,
      password: e.target.value,
      score: passStrength.score,
      suggestions: passStrength.feedback.suggestions
    }))
  }

  return (
    <div>
      <form>
        <ZxcvbInput handleChange={handleChange}/>
        <ZxcvbnSuggestions suggestions={zxcvbnData.suggestions} length={zxcvbnData.password.length} />
        <PassStrenghtMeter pass={zxcvbnData.score} />
      </form>
    </div>
  );
}

export default ZxcvbnPassword;
