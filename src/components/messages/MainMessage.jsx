import React from 'react'
import { useNotification } from './MessageProvider'
import message from './message.json'


function MainMessage() {

    const dispatch = useNotification();
    console.log(message.Messages.warning.type)
    const handleInfo = () => {
        // this is the payload
        dispatch(message.Messages.info)
    }

    const handleSuccess = () => {
        // this is the payload
        dispatch(message.Messages.success)
    }


    const handleWarning = () => {
        // this is the payload
        dispatch(message.Messages.warning)
    }

   

    return (
        <div>
            <button className='message-button' onClick={handleInfo} >Add Info message</button>
            <button className='message-button' onClick={handleSuccess} >Add success message</button>
            <button className='message-button' onClick={handleWarning} >Add warning message</button>
            <button className='message-button' onClick={()=> dispatch(message.Messages.error)} >Add error message</button>
        </div>
    )
}

export default MainMessage