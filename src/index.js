import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css'
import MessageProvider from "./components/messages/MessageProvider"

ReactDOM.render(
 
    <MessageProvider>
    <App />
    </MessageProvider>,
  document.getElementById('root')
);

